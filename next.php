<?
define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));
$title = "The Greatest Truth Ever Told";
try {
    if (!getenv('OPENSHIFT_MYSQL_DB_HOST')) {
        $conn = new PDO("mysql:unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock;dbname=eitan", "root", "root");
    } else {
        $conn = new PDO("mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
    }
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
?>
<div class="messages">
    <?
    $page = (int) (((intval($_GET['page']) - 1) * 10));
    $next_page = intval($_GET['page']) + 1;
    $stmt = $conn->prepare("SELECT * FROM messages ORDER BY id DESC LIMIT 10 OFFSET :page;");
    $stmt->bindParam(':page', $page, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $receivers = array(
        "1" => "Eitan",
        "2" => "Elana"
    );
    $aligns = array(
        "1" => "left",
        "2" => "right"
    );
    foreach ($result as $index => $doc) {
        echo "<div class=\"message\">";
            echo "<div class=\"message_inner message_inner_" . $aligns[$doc['receiver']] . "\">";
                echo "<h3>To: " . $receivers[$doc['receiver']] . "</h3>";
                echo "<p>" . htmlspecialchars($doc['content']) . "</p>";
            echo "</div>";
        echo "</div>";
    }
    if (count($result) == 10) {
        echo "<div class=\"page_link_wrap\"><a href=\"/next.php?page={$next_page}\">Load More</a></div>";
    }
    ?>
</div>
