<?
define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));
$title = "The Greatest Truth Ever Told";
try {
    if (!getenv('OPENSHIFT_MYSQL_DB_HOST')) {
        $conn = new PDO("mysql:unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock;dbname=eitan", "root", "root");
    } else {
        $conn = new PDO("mysql:host=" . DB_HOST . ";port=" . DB_PORT . ";dbname=" . DB_NAME, DB_USER, DB_PASS);
    }
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo 'Connection failed: ' . $e->getMessage();
}
if (!empty($_POST['content']) && !empty($_POST['receiver'])) {
    if ($_POST['receiver'] == "1" || $_POST['receiver'] == "2") {
        $stmt = $conn->prepare("INSERT INTO messages (content, receiver)
    VALUES (:content, :receiver)");
        $stmt->bindParam(':content', $_POST['content']);
        $stmt->bindParam(':receiver', $_POST['receiver']);
        $stmt->execute();
        $res = "<p class=\"highlight\">Message sent!</p>";
    }
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?=$title?></title>
        <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="./main.css" media="screen" title="no title" charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="./jquery.jscroll.min.js" charset="utf-8"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#messages').jscroll({
                    nextSelector: '.page_link_wrap a:last',
                    autoTrigger: false
                });
            });
        </script>
    </head>
    <body>
        <div class="wrap">
            <div class="header">
                <a href="/"><h1><?=$title?></h1></a>
            </div>
            <div class="body">
                <h4>New Message</h4>
                <div class="sep"></div>
                <form class="send_form" method="post" action="/">
                    <?
                    if ($res) {
                        echo $res;
                    }
                    ?>
                    <table>
                        <tr>
                            <td>
                                Send To:
                            </td>
                            <td>
                                <select class="form_receiver" name="receiver">
                                    <option value="1">Eitan</option>
                                    <option value="2">Elana</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Message:
                            </td>
                            <td>
                                <textarea name="content" rows="8" cols="40"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit" class="large">Send</button>
                            </td>
                        </tr>
                    </table>
                </form>
                <h4>Messages <a href="/" style="float: right">Refresh</a></h4>
                <div class="sep"></div>
                <div id="messages">
                    <?
                    $stmt = $conn->prepare("SELECT * FROM messages ORDER BY id DESC LIMIT 10 OFFSET 0;");
                    $stmt->bindParam(':username', $_SERVER['PHP_AUTH_USER']);
                    $stmt->execute();
                    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $receivers = array(
                        "1" => "Eitan",
                        "2" => "Elana"
                    );
                    $aligns = array(
                        "1" => "left",
                        "2" => "right"
                    );
                    foreach ($result as $index => $doc) {
                        echo "<div class=\"message\">";
                            echo "<div class=\"message_inner message_inner_" . $aligns[$doc['receiver']] . "\">";
                                echo "<h3>To: " . $receivers[$doc['receiver']] . "</h3>";
                                echo "<p>" . htmlspecialchars($doc['content']) . "</p>";
                            echo "</div>";
                        echo "</div>";
                    }
                    ?>
                    <div class="page_link_wrap">
                        <a href="/next.php?page=2">Load More</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<?
$conn = null;
?>
